//
//  ViewController.swift
//  AutoLayoutTips
//
//  Created by See.Ku on 2015/01/10.
//  Copyright (c) 2015 AxeRoad. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBAction func onSubviewOn(sender: AnyObject) {
		g_subviewAutoLayoutViewController = true
		performSegueWithIdentifier("AutoLayout", sender: self)
	}

	@IBAction func onSubviewOff(sender: AnyObject) {
		g_subviewAutoLayoutViewController = false
		performSegueWithIdentifier("AutoLayout", sender: self)
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

}

// eof

