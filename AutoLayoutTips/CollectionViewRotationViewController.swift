//
//  CollectionViewRotationViewController.swift
//  AutoLayoutTips
//
//  Created by See.Ku on 2015/05/06.
//  Copyright (c) 2015 AxeRoad. All rights reserved.
//

import UIKit

class CollectionViewRotationViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var changeFunc: UISegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()

		collectionView.dataSource = self
		collectionView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

	////////////////////////////////////////////////////////////////
	// MARK: - レイアウトをやり直し

	override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
		super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)

		if changeFunc.selectedSegmentIndex == 0 {
			collectionView.collectionViewLayout.invalidateLayout()
			collectionView.reloadData()
		}
	}

	override func viewDidLayoutSubviews() {

		if changeFunc.selectedSegmentIndex == 1 {
			collectionView.collectionViewLayout.invalidateLayout()
			collectionView.reloadData()
		}

		super.viewDidLayoutSubviews()
	}

	////////////////////////////////////////////////////////////////
	// MARK: - UICollectionViewDataSource

	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

		// とりあえず、8個で固定
		return 8
	}

	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! UICollectionViewCell
		let noLabel = cell.viewWithTag(1) as! UILabel
		let sizeLabel = cell.viewWithTag(2) as! UILabel

		// Cellの番号とサイズを表示
		noLabel.text = "No.\(indexPath.item)"

		let wx = Int(cell.bounds.width)
		let wy = Int(cell.bounds.height)
		sizeLabel.text = "(\(wx)x\(wy))"

		return cell
	}

	////////////////////////////////////////////////////////////////
	// MARK: - UICollectionViewDelegateFlowLayout

	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {

		// collectionViewを上下とも3等分
		let base = collectionView.bounds.size
		return CGSize(width: base.width/3, height: base.height/3)
	}

}

// eof
