//
//  SpecifiedCenterViewController.swift
//  AutoLayoutTips
//
//  Created by See.Ku on 2015/01/10.
//  Copyright (c) 2015 AxeRoad. All rights reserved.
//

import UIKit

class SpecifiedCenterViewController: UIViewController {

	@IBOutlet weak var textView: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()

		textView.text = "frame: \(textView.frame)"
	}

}

// eof
