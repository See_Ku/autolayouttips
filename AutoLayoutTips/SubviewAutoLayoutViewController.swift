//
//  SubviewAutoLayoutViewController.swift
//  AutoLayoutTips
//
//  Created by See.Ku on 2015/04/27.
//  Copyright (c) 2015 AxeRoad. All rights reserved.
//

import UIKit

var g_subviewAutoLayoutViewController = false

func autoLayoutOff(view: UIView) {

	// 制約を全て削除
	let ar = view.constraints()
	view.removeConstraints(ar)

	// 基準になるViewは自動変換を使わない
	view.setTranslatesAutoresizingMaskIntoConstraints(false)

	// 子Viewは自動変換をONに
	for child in view.subviews {
		if let child = child as? UIView {
			child.setTranslatesAutoresizingMaskIntoConstraints(true)
		}
	}
}

class SubviewAutoLayoutViewController: UIViewController {

	@IBOutlet weak var baseView: UIView!
	@IBOutlet weak var testLabel: UILabel!
	@IBOutlet weak var testText: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()

		if g_subviewAutoLayoutViewController == false {
			navigationItem.title = "AutoLayout OFF"

			// baseViewの中だけ、AutoLayoutをオフにする
			autoLayoutOff(baseView)

		} else {
			navigationItem.title = "AutoLayout ON"
		}
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()

		// AutoLayout ON + frameを直接変更
		//	表示に反映されるのが一瞬遅れる
		//	画面の回転で元に戻る

		// AutoLayout OFF + frameを直接変更
		//	特に問題なし

		let re = baseView.bounds
		testLabel.frame = CGRect(x: 10, y: 10, width: re.width-20, height: 30)
		testText.frame = CGRect(x: 10, y: 50, width: re.width-20, height: re.height-60)
		baseView.layoutIfNeeded()
	}
}

// eof
